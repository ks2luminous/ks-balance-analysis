#!/usr/bin/env python
# coding: utf-8

# In[1]:


# the expected win rate predictor is designed to be as high performance and stable as possible


# In[1]:


import pandas as pd
import os
import numpy as np
from tqdm import tqdm
import pickle
from datetime import datetime, timedelta
from collections import defaultdict

import pymysql
from sqlalchemy import create_engine
from ks_constants.roles import Role, Team
from ks_constants.regions import Region
from pandas import Timestamp


# In[2]:
DATA_DIR = "../data"
if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)


def read_from_data_dir(name):
    return os.path.join(DATA_DIR, name)


# In[104]:


# this only runs on Lucifer
connection_info = {
    'username': 'root',
    'host': 'host.docker.internal',
    'database': 'ks_prod'
}
# public as the db machine is not accessible via internet (in theory...)
engine = create_engine(f"mysql+pymysql://{connection_info['username']}:{os.environ['MYSQL_RW_PASSWORD']}@"
                      f"{connection_info['host']}/{connection_info['database']}", pool_recycle=600)

def get_pandas(query):
    with engine.connect() as dbConnection:
        return pd.read_sql(query, dbConnection, params=None)
        
players = get_pandas("select * from players where functional_role != 'Random' and functional_role is not NULL")
del players["team_int"]
del players["win_status"]

games = get_pandas("select * from games where outcome in (0, 1)")
handles = get_pandas("select * from handles")

PREDICTION_SET_SIZE = 3

# In[4]:


players = players[players["functional_role"] != "Random"]
players["internal_role"] = players["internal_role"].fillna(players["functional_role"])
players["functional_role"] = players["functional_role"].apply(lambda x: Role[x])
players["internal_role"] = players["internal_role"].apply(lambda x: Role[x])
players["team"] = players["functional_role"].apply(lambda x: x.get_team())
games["region"] = games["region"].apply(lambda x: Region[x])

del handles["ds"]

mmr_games = players.merge(games, on="game_id").merge(handles, on="player_handle", how="left")
mmr_games['battle_tag'].fillna(mmr_games["player_handle"], inplace=True)
mmr_games = mmr_games.rename({"battle_tag": "identity"}, axis=1)


# In[5]:


len(set(mmr_games[mmr_games["functional_role"] == Role.Critter_Lord]["game_id"]))


# In[6]:


assert len(set(games["game_id"])) == games.shape[0]


# In[7]:


game_table = mmr_games.groupby("game_id").agg({"pregame_mmr": list, "identity": list, "team": list, "outcome": max, "functional_role": list, "datetime_of_game": max}).reset_index()


# In[8]:


game_table["team_sum"] = game_table["team"].apply(sum)
game_table = game_table[game_table["team_sum"] == 2]
game_table["datetime_of_game"] = pd.to_datetime(game_table["datetime_of_game"])
game_table = game_table.sort_values("datetime_of_game")


# In[9]:



def k_function(num_games_played):
    return max(1 / (10 + num_games_played) * 1000, 10)

def elo(diff):
    return 1. / (1 + 10 ** (-diff / 400))


# In[291]:


DEFAULT_MMR = {Team.Survivor: 900, Team.Kerrigan: 700}

class Player:
    def __init__(self, identifier):
        self._id = identifier
        self._core_seed = defaultdict(list)
        self._core_mmr = dict(zip([team.name for team in Team], [DEFAULT_MMR] * len(Team)))
        self._role_mmr = defaultdict(int)
        self._core_games_played = defaultdict(int)
        self._role_games_played = defaultdict(int)
        # internally, we store it as a int because we get a pickling error trying to store a timestamp (timezone issue?)
        self._most_recent_game = defaultdict(int)

    def set_core_seed(self, role: Role, mmr):
        self._core_seed[role.get_team().name].append(mmr)

    def finalize_seeding(self):
        if self._core_seed is None:
            raise RuntimeError(f"Finalize Seeding failed for {self._id} because _core_seed is null. Are you finalizing seeding twice?")

        for core in Team:
            core_games = len(self._core_seed[core.name])
            has_games = core_games > 0
            
            if has_games:
                approx_mmr = max(min(np.max(self._core_seed[core.name]) - 300, 3000), np.mean(self._core_seed[core.name]))
            # if not many games played, use a modified default, unless approximate mmr is much higher than we see
            if has_games and (core_games >= 5 or approx_mmr >= 1300):
                self._core_mmr[core.name] = approx_mmr
            else:
                self._core_mmr[core.name] = DEFAULT_MMR[core]

        # we can clear out core seed to reduce memory req
        self._core_seed = None

    def get_core_mmr(self, core: Team):
        assert 'Survivor' in self._core_mmr
        return self._core_mmr[core.name]

    def modify_core_mmr(self, core: Team, adjustment: float):
        self._core_mmr[core.name] += adjustment

    def get_role_mmr(self, role: Role):
        if role == Role.Random:
            # random's core is both
            return np.mean(list(self._core_mmr.values())) + self._role_mmr[role.name]
        return self._core_mmr[role.get_team().name] + self._role_mmr[role.name]

    def get_role_delta_mmr(self, role: Role):
        return self._role_mmr[role.name]

    def add_result(self, role: Role, team_mmr: float, opponent_mmr: float, win):
        outcome = 1 if win else 0
        expected = elo(team_mmr - opponent_mmr)
        core = role.get_team()
        k_core = k_function(self._core_games_played[core.name])
        k_role = k_function(self._role_games_played[role.name])

        self._core_mmr[core.name] += (outcome - expected) * k_core
        self._role_mmr[role.name] += (outcome - expected) * k_role

        self._core_games_played[core.name] += 1
        self._role_games_played[role.name] += 1

    def get_games_played(self):
        return sum(self._core_games_played.values())

    def get_games_played_on_class(self, role: Role):
        return self._role_games_played[role.name]

    def set_most_recent_game(self, role: Role, time: Timestamp) -> None:
        self._most_recent_game[role.name] = int(time.timestamp())

    def get_most_recent_game_for_role(self, role: Role) -> Timestamp:
        return Timestamp.fromtimestamp(self._most_recent_game[role.name])

    def get_most_recent_game(self) -> Timestamp:
        return Timestamp.fromtimestamp(max(self._most_recent_game.values()))

    def get_identity(self):
        return self._id

    def get_mmrs(self):
        data = []
        for role in Role:
            if self.get_games_played_on_class(role) > 0:
                data.append((role.get_english_name(), self.get_role_mmr(role)))

        return pd.DataFrame(data, columns=["role_name", "mmr"])
            
# simple test
player = Player("lumi")
# set core repeatedly since code checks sample size in seeding
for i in range(10):
    player.set_core_seed(Role.Scientist, 1400)
    player.set_core_seed(Role.Dehaka, 1450)
player.finalize_seeding()
print(player.get_core_mmr(Team.Kerrigan))
assert player.get_core_mmr(Team.Kerrigan) == 1450

# lose to equally matched opponent as kerrigan
player.add_result(Role.Kerrigan, 1450, 1450, 0)
# should lose mmr equally

assert player.get_role_mmr(Role.Kerrigan) - player.get_core_mmr(Team.Kerrigan) == player.get_core_mmr(Team.Kerrigan) - 1450

new_core = player.get_core_mmr(Team.Kerrigan)
# lose to another 1450 as dehaka
player.add_result(Role.Dehaka, 1400, 1450, 0)

# gap between dehaka and core should exceeed core from original core
assert abs(player.get_role_mmr(Role.Dehaka) - player.get_core_mmr(Team.Kerrigan)) > abs(player.get_core_mmr(Team.Kerrigan) - new_core)


# In[292]:


# 3 passes:
# 1. create players
# 2. seed players
# 3. update mmrs

MISSING_PLAYER_MMR = 700

# 1st pass
players = {}
for player in set(mmr_games["identity"]):
    players[player] = Player(player)


def seed_mmrs(row):
    for player, role, mmr in zip(row["identity"], row["functional_role"], row["pregame_mmr"]):
        if role != Role.Random:
            players[player].set_core_seed(role, mmr)
        
# 2nd pass
game_table.apply(seed_mmrs, axis=1)
    
for player in players.values():
    player.finalize_seeding()

# 3rd pass
def get_teams_in_row(row):
    teams = row["team"]
    players = row["identity"]
    surv_players = []
    kerri_players = []
    for player, role, team_int in zip(players, row["functional_role"], teams):
        if role != Role.Random:
            # apparently it is possible for the role to be not None and to not have a team int?
            if team_int == Team.Survivor:
                surv_players.append((player, role))
            elif team_int == Team.Kerrigan:
                kerri_players.append((player, role))

    return surv_players, kerri_players

revised_game_rows = []

def update_mmrs(row):
    surv_players, kerri_players = get_teams_in_row(row)

    for player, _ in surv_players + kerri_players:
        # if we find a new player, create
        if player not in players:
            assert type(player) == str
            print("Did not find Player, Creating anew", player)
            self.add_player(MMR.Player(player))

    surv_mmrs = [players[player].get_role_mmr(role) for player, role in surv_players]
    surv_roles = [role for _, role in surv_players]
    while len(surv_mmrs) < 8:
        surv_mmrs.append(MISSING_PLAYER_MMR)
    kerri_mmrs = [players[player].get_role_mmr(role) for player, role in kerri_players]
    kerri_roles = [role for _, role in kerri_players]
    surv_num_games = [players[player].get_games_played_on_class(role) for player, role in surv_players]
    kerri_num_games = [players[player].get_games_played_on_class(role) for player, role in kerri_players]

    if 0 < len(kerri_mmrs) < 2:
        kerri_mmrs.append(kerri_mmrs[0])

    # need to filter these out in db!
    if len(kerri_mmrs) == 0:
        return

    surv_team_mmr = np.mean(surv_mmrs) if len(surv_mmrs) > 0 else 0
    kerri_team_mmr = np.mean(kerri_mmrs) if len(kerri_mmrs) > 0 else 0

    assert type(row["datetime_of_game"]) == Timestamp
    if len(surv_mmrs) > 0 and len(kerri_mmrs) > 0:
        for player, role in surv_players:
            players[player].add_result(role, surv_team_mmr, kerri_team_mmr, row['outcome'] == Team.Survivor)
            # update time of last played
            players[player].set_most_recent_game(role, row["datetime_of_game"])

        for player, role in kerri_players:
            players[player].add_result(role, kerri_team_mmr, surv_team_mmr, row['outcome'] == Team.Kerrigan)
            players[player].set_most_recent_game(role, row["datetime_of_game"])

    # note the roles and mmrs wont necessarily match in length if there are roles being subbed in or solo kerri
    return surv_mmrs, surv_roles, kerri_mmrs, kerri_roles, row['outcome'], row["datetime_of_game"], surv_num_games, kerri_num_games
    
tqdm.pandas(desc="Updating mmrs")

core_mmr_games_table = game_table.progress_apply(update_mmrs, axis=1)


# In[293]:


lucy_mmr_table = pd.DataFrame(revised_game_rows, columns=["game_id", "identity", "mmr"])


# In[294]:


with open(read_from_data_dir("players.pkl"), "wb") as f:
    pickle.dump(players, f)
    
lucy_mmr_table.to_csv(read_from_data_dir("core_mmr_games.csv"), index=False)


# In[295]:


surv_mmrs = []
surv_roles = []
kerri_mmrs = []
kerri_roles = []
outcomes = []
datetimes = []
surv_num_games = []
kerri_num_games = []
for row in core_mmr_games_table:
    if len(row[2]) > 0:
        surv_mmrs.append(row[0])
        surv_roles.append(row[1])
        kerri_mmrs.append(row[2])
        kerri_roles.append(row[3])
        outcomes.append(row[4])
        datetimes.append(row[5])
        surv_num_games.append(row[6])
        kerri_num_games.append(row[7])
    
import pickle
with open("core_mmrs.pkl", "wb") as f:
    pickle.dump((surv_mmrs, kerri_mmrs, outcomes), f)


# In[296]:


for role in Role:
    print(players["Ribby#2952"].get_role_mmr(role))


# In[297]:


from keras.layers import *
from keras.models import Model
from keras.callbacks import *
from keras import regularizers
import keras
import keras.backend as K

from sklearn.preprocessing import StandardScaler


# In[298]:


input_surv_mmrs = np.array(surv_mmrs)
input_kerri_mmrs = np.array(kerri_mmrs)
labels = np.array(outcomes)


# In[299]:


surv_mmr_transformer = StandardScaler()
kerri_mmr_transformer = StandardScaler()

scaled_surv_mmrs = surv_mmr_transformer.fit_transform(input_surv_mmrs)
scaled_kerri_mmrs = kerri_mmr_transformer.fit_transform(input_kerri_mmrs)


# In[300]:


combined_inputs = np.concatenate([scaled_surv_mmrs, scaled_kerri_mmrs], axis=1)
transposed_inputs = combined_inputs.transpose()


# In[301]:


POLY_ORDER = 1

full_matrix = np.zeros((10, len(input_surv_mmrs), POLY_ORDER + 1))

def polynomialize(x):
    return np.array([x, x ** 2])

for i in range(full_matrix.shape[0]):
    for j in range(full_matrix.shape[1]):
        full_matrix[i][j] = polynomialize(transposed_inputs[i][j])


# In[310]:


# we don't use data from day 0 because mmr was lower quality then
front_trim = int(full_matrix.shape[1] * 0.1)
split = int(full_matrix.shape[1] * 0.85)
train_matrix = full_matrix[:, front_trim : split, :]
valid_matrix = full_matrix[:, split:, :]
train_labels = labels[front_trim : split]
valid_labels = labels[split:]


# In[311]:


def train_and_predict():
    K.clear_session()

    # This returns a tensor
    surv_team_input = [Input(shape=(POLY_ORDER + 1,)) for x in range(input_surv_mmrs.shape[1])]
    kerri_team_input = [Input(shape=(POLY_ORDER + 1,)) for x in range(input_kerri_mmrs.shape[1])]

    # a layer instance is callable on a tensor, and returns a tensor
    surv_team_univariate_function = Dense(1, activation='linear')
    kerri_team_univariate_function = Dense(1, activation='linear')

    surv_team_strength = Add()([surv_team_univariate_function(inp) for inp in surv_team_input])
    kerri_team_strength = Add()([kerri_team_univariate_function(inp) for inp in kerri_team_input])
    output = Dense(1, activation='sigmoid')(Subtract()([surv_team_strength, kerri_team_strength]))

    # This creates a model that includes
    # the Input layer and three Dense layers
    model = Model(inputs=surv_team_input + kerri_team_input, outputs=output)
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])

    callbacks = [
        EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5),
        ModelCheckpoint('best_model.h5', monitor='val_loss', mode='min', verbose=1, save_best_only=True)
    ]
    model.fit(x=list(train_matrix), 
              y=train_labels,
              epochs=100,
              shuffle=True,
              callbacks=callbacks,
              validation_data=(list(valid_matrix), valid_labels),
              batch_size=256
              )  # starts training

    model.load_weights("best_model.h5")

    return model.predict(list(full_matrix), batch_size=128).flatten(), callbacks[0].__dict__['best']


# In[312]:


# Balance Framework


# In[313]:


ACCEPTABLE_LOSS_THRESHOLD = 0.56
agg_predictions = []
for mult_iters in range(PREDICTION_SET_SIZE):
    predictions, val_loss = train_and_predict()
    if val_loss < ACCEPTABLE_LOSS_THRESHOLD:
        agg_predictions.append(predictions)

if len(agg_predictions) > 1:
    predicted_outcomes = np.mean(agg_predictions, axis=0)
else:
    predicted_outcomes = agg_predictions[0]


# In[314]:


balance_table = pd.DataFrame(zip(surv_mmrs, 
                                 surv_roles, 
                                 kerri_mmrs, 
                                 kerri_roles, 
                                 outcomes, 
                                 predicted_outcomes, 
                                 datetimes, 
                                 surv_num_games, 
                                 kerri_num_games), 
                             columns=
                             [
                                 "survivor_mmrs", 
                                 "survivor_roles", 
                                 "kerrigan_mmrs", 
                                 "kerrigan_roles", 
                                 "outcome", 
                                 "predicted", 
                                 "datetime_of_game", 
                                 "surv_games_played", 
                                 "kerri_games_played"
                             ])

with open(read_from_data_dir('full_balance.pkl'), 'wb') as f:
    pickle.dump(balance_table, f)


# In[315]:


latest_date = balance_table["datetime_of_game"].max()
balance_subset = balance_table[balance_table["datetime_of_game"] > (latest_date - timedelta(days=60))]


# In[316]:


kerri_bias = np.mean(balance_subset["predicted"]) - np.mean(balance_subset["outcome"])
print("Kerri Bias", kerri_bias)


# In[321]:


# filtering criteria for when a role is considered to have been played in a game for balance purposes

MIN_GAMES = 0
def classes_counted(row):
    counted_for = []
    for role, count in zip(row["survivor_roles"] + row["kerrigan_roles"],
                          row["surv_games_played"] + row["kerri_games_played"]):
        if count >= MIN_GAMES:
            counted_for.append(role)
    return counted_for

balance_subset["roles_counted"] = balance_subset.apply(classes_counted, axis=1)


# In[322]:


pick_class = Role.Brakk
class_is_in = balance_subset["roles_counted"].map(lambda x: pick_class in x)
with_1 = balance_subset[class_is_in]
with_0 = balance_subset[~class_is_in]

with_1


# In[323]:


def balance_stat(pick_class: Role):
    class_is_in = balance_subset["roles_counted"].map(lambda x: pick_class in x)
    with_1 = balance_subset[class_is_in]
    with_0 = balance_subset[~class_is_in]
    
    role_is_on_kerrigan = pick_class.get_team() == Team.Kerrigan
    
    
    diff_with_1 = -1 * (with_1["outcome"].mean() - with_1["predicted"].mean()) - kerri_bias
    if role_is_on_kerrigan:
        diff_with_1 = -diff_with_1
    #diff_with_0 = with_0["predicted"].mean() - with_0["outcome"].mean()

    win_rate = with_1["outcome"].mean() if role_is_on_kerrigan else (1 - with_1["outcome"].mean())
    
    expected_win_rate = with_1["predicted"].mean() if role_is_on_kerrigan else (1 - with_1["predicted"].mean())
    
    return diff_with_1, len(with_1), len(with_0), win_rate, expected_win_rate

balance_data = []
for role in Role:
    if role != Role.Random: # we have a bug somewhere, it should never be Random
        relative_win_rate, num_with_1, num_with_0, win_rate, expected_win_rate = balance_stat(role)
        balance_data.append([role.get_english_name(), "{:.4f}".format(relative_win_rate * 100) + "%", num_with_1, 
                             num_with_0, 
                             "{:.4f}".format(win_rate * 100) + "%",
                             "{:.4f}".format(expected_win_rate * 100) + "%",
                            ])


# In[324]:


df = pd.DataFrame(balance_data, columns=["Class", 
                                         "Relative Win Rate", 
                                         "Games with 1+ of Class", 
                                         "Games with 0 of Class", 
                                         "Win Rate of Class",
                                         "Expected Win Rate of Games with 1+ of Class"
                                        ])

df.to_csv(read_from_data_dir("balance_output.csv"), index=False)
df.style.hide(axis='index')


# In[ ]:





# In[ ]:




